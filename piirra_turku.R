# Piirrä kuva
library(plyr)
library(tidyverse)
library(forcats)
library(hrbrthemes)
# library(showtext)
library(cowplot)
library(extrafont)
library(svglite)
library(ggplot2)

loadfonts()
fontti <- "Roboto Condensed"

# Sys.sleep(55)
library(jsonlite)
df <- jsonlite::fromJSON(txt = "http://data.foli.fi/citybike/pretty", simplifyDataFrame = TRUE)[[1]]
dff <- ldply (df, data.frame)

# Yhden alueen nimi väärin, korjataan
# dff$name <- revalue(dff$name, c("kupittaan asema"="Popup 2"))
dff$networks <- NULL
dff$time <- Sys.time()
dff %>% 
  mutate(share = round(bikes_avail/slots_total*100,1)) -> dat_rt

dd <- readRDS("./data/df16_turku2.RDS") %>%
  filter(KUNTAKOODI == "853") %>% mutate(lat=60.45, long=22.26) %>% 
  saveRDS("./data/tiet_turku.RDS")



# datd <- dat_rt
# 
# loop_data <- data_frame(nimi = c("Turku"),
#                         # Rautatieasema
#                         pisteet = c(unlist(c(dat_rt$name))
#                         ),
#                         latmax = NA,
#                         latmin = NA,
#                         lonmax = NA,
#                         lonmin = NA)
# 
# namess <- dat_rt$name
# loop_data$name <- namess
# 
# for (i in 1:nrow(loop_data)){
#   # subsetataan ko. telineet
#   dtmp <- dat_rt[dat_rt$name %in% loop_data[[i,7]],]
#   # määritetään siitä koordinaatit karttapohjan subsettaukseen
#   loop_data[i, ]$latmax <- max(dtmp$lon) * 1.00002
#   loop_data[i, ]$latmin <- min(dtmp$lon) * 0.99998
#   loop_data[i, ]$lonmax <- max(dtmp$lat) * 1.00005
#   loop_data[i, ]$lonmin <- min(dtmp$lat) * 0.99995
#   
# }


datd <- dat_rt

datd$name <- fct_reorder(datd$name, datd$bikes_avail)

# Tolpat
p <- ggplot(datd)
p <- p + geom_col(aes(x=name,y=slots_total), fill="grey85", alpha=.5)
p <- p + geom_col(aes(x=name,y=bikes_avail, fill=share), show.legend=FALSE)
p <- p + scale_fill_distiller(palette = "RdYlGn", direction = 1, limit=c(1,100))
p <- p + labs(title=paste("Vapaat fillarit paikan Turku lähellä"),
              subtitle = paste("Tilanne klo:",format(Sys.time(),"%H:%M:%S %Ana %d. %Bta %Y")),
              x=NULL,
              y=NULL)
p <- p + coord_flip()
p <- p + theme_ipsum_rc(grid = FALSE, base_family = fontti, base_size = 16)
p <- p + theme(axis.text.x = element_blank(),
               axis.text.y = element_text(face ="bold"))
p <- p + geom_hline(yintercept=seq(1,100, by=1), color = "white")
p1 <- p + geom_label(aes(x=name,y=bikes_avail,label=bikes_avail,fill=share),color="white", nudge_y=-.5, size=5, family=fontti, fontface="bold", show.legend = FALSE)

mp <- readRDS("./data/tiet_turku.RDS")
# rak <- kelagis::get_geo(year = 2017, level = "rakennukset")
rak <- readRDS("./data/turku_tiet2.RDS")
rak2 <- readRDS("./data/df17_turku_po2.RDS")
# sf::st_coordinates(rak) %>%
 # as_data_frame() -> testi
# sf::st_cast(rak$geometry, "POINT") -> kaik
  
# rak <- rak %>% filter((long>=22.21 & long<=22.34) & (lat<=60.47 & lat>=60.425))
# rak2 <- rak2 %>% filter((long>=22.21 & long<=22.34) & (lat<=60.48 & lat>=60.42))
p <- ggplot(data=mp, aes(x=long,y=lat))
p <- p + geom_path(color="dim grey", size=2, alpha=.2)
p <- p + geom_polygon(data=rak, aes(x=long,y=lat,group=id), fill=NA, color="grey80", alpha=.2)
p <- p + geom_polygon(data=rak2, aes(x=long,y=lat,group=id), fill=NA, color="black", alpha=.2)
p <- p + geom_point(data=datd, aes(x=lon,y=lat, color=share, group=name), size=15, alpha=.8, show.legend = FALSE)
p <- p + geom_text(data=datd, aes(x=lon,y=lat, label=bikes_avail, group=name), size=6, color="white", family=fontti)
p <- p + geom_label(data=datd, aes(x=lon,y=lat, label=name, group=name), size=4, color="black", fill="white", alpha=.6, nudge_y = .0005, family=fontti)
p <- p + scale_color_distiller(palette = "RdYlGn", direction = 1, limit=c(1,100))
p <- p + coord_map(project="orthographic")
p <- p + theme_ipsum_rc(grid = FALSE)
p <- p + coord_cartesian(ylim=c(60.425,60.47), xlim=c(22.21,22.34)) #zoomaus
p2 <- p + theme(axis.text = element_blank(),
                axis.title = element_blank())
p2

# Aikasarja
# Lataa pitkittäisdata
readRDS("./foli_rental_cumulative.RDS")  %>%
  mutate(share = round(bikes_avail/slots_total*100,1)) -> dat_ts

datf <- dat_ts
datf2 <- datf[datf$time >= Sys.time()-36000,]

datf$name <- factor(datf$name, levels=levels(datd$name))
datf$name <- fct_rev(datf$name)
datf <- datf %>% filter(name %in% c("Kupittaan asema","Kaupungintalo","Satama"))


p <- ggplot(datf)
p <- p + geom_area(aes(x=time,y=slots_total),fill="grey95", alpha=.6)
p <- p + geom_area(aes(x=time,y=bikes_avail),fill="#81b33a")
p <- p + facet_wrap(~name, scales = "free")
p <- p + theme_ipsum_rc(base_family = fontti, grid = FALSE)
p <- p + labs(subtitle = "Viimeiset 10 tuntia",
              y="Vapaiden pyörien lukumäärä",
              x="Kellonaika",
              caption=format(Sys.time(),"%H:%M:%S %Ana %d. %Bta %Y"))
p3 <- p + geom_hline(yintercept=seq(1,max(datf$slots_total), by=1, size=.2), color = "white")
p3
plot <- ggdraw() +
  draw_plot(p3,  0, 0,  1, .5) +
  draw_plot(p2,  0,  .5,  .5, .5) +
  draw_plot(p1, .5,  .5, .5, .5)
plot


# Seuraavat rajauksia eri alueille. Vain kokeiluja


# Rajaus vain sataman alueelle
# p <- ggplot(data=mp, aes(x=long,y=lat))
# p <- p + geom_path(color="dim grey", size=2, alpha=.2)
# p <- p + geom_polygon(data=rak, aes(x=long,y=lat,group=id), fill=NA, color="grey80", alpha=.2)
# p <- p + geom_polygon(data=rak2, aes(x=long,y=lat,group=id), fill=NA, color="black", alpha=.2)
# p <- p + geom_point(data=datd, aes(x=lon,y=lat, color=share, group=name), size=15, alpha=.8, show.legend = FALSE)
# p <- p + geom_text(data=datd, aes(x=lon,y=lat, label=bikes_avail, group=name), size=6, color="white", family=fontti)
# p <- p + geom_label(data=datd, aes(x=lon,y=lat, label=name, group=name), size=4, color="black", fill="white", alpha=.6, nudge_y = .0005, family=fontti)
# p <- p + scale_color_distiller(palette = "RdYlGn", direction = 1, limit=c(1,100))
# p <- p + coord_map(project="orthographic")
# p <- p + theme_ipsum_rc(grid = FALSE)
# p <- p + coord_cartesian(ylim=c(60.43,60.45), xlim=c(22.21,22.245)) #zoomaus
# p4 <- p + theme(axis.text = element_blank(),
#                 axis.title = element_blank())
# p4

# Keskusta
# p <- ggplot(data=mp, aes(x=long,y=lat))
# p <- p + geom_path(color="dim grey", size=2, alpha=.2)
# p <- p + geom_polygon(data=rak, aes(x=long,y=lat,group=id), fill=NA, color="grey80", alpha=.2)
# p <- p + geom_polygon(data=rak2, aes(x=long,y=lat,group=id), fill=NA, color="black", alpha=.2)
# p <- p + geom_point(data=datd, aes(x=lon,y=lat, color=share, group=name), size=15, alpha=.8, show.legend = FALSE)
# p <- p + geom_text(data=datd, aes(x=lon,y=lat, label=bikes_avail, group=name), size=6, color="white", family=fontti)
# p <- p + geom_label(data=datd, aes(x=lon,y=lat, label=name, group=name), size=4, color="black", fill="white", alpha=.6, nudge_y = .0005, family=fontti)
# p <- p + scale_color_distiller(palette = "RdYlGn", direction = 1, limit=c(1,100))
# p <- p + coord_map(project="orthographic")
# p <- p + theme_ipsum_rc(grid = FALSE)
# p <- p + coord_cartesian(ylim=c(60.44,60.46), xlim=c(22.24,22.275)) #zoomaus
# p5 <- p + theme(axis.text = element_blank(),
#                 axis.title = element_blank())
# p5


# Itäinen keskusta (eli täl puol jokee eli tuomiokirkko, Hämeenkatu ja Turun yliopisto)
# p <- ggplot(data=mp, aes(x=long,y=lat))
# p <- p + geom_path(color="dim grey", size=2, alpha=.2)
# p <- p + geom_polygon(data=rak, aes(x=long,y=lat,group=id), fill=NA, color="grey80", alpha=.2)
# p <- p + geom_polygon(data=rak2, aes(x=long,y=lat,group=id), fill=NA, color="black", alpha=.2)
# p <- p + geom_point(data=datd, aes(x=lon,y=lat, color=share, group=name), size=15, alpha=.8, show.legend = FALSE)
# p <- p + geom_text(data=datd, aes(x=lon,y=lat, label=bikes_avail, group=name), size=6, color="white", family=fontti)
# p <- p + geom_label(data=datd, aes(x=lon,y=lat, label=name, group=name), size=4, color="black", fill="white", alpha=.6, nudge_y = .0005, family=fontti)
# p <- p + scale_color_distiller(palette = "RdYlGn", direction = 1, limit=c(1,100))
# p <- p + coord_map(project="orthographic")
# p <- p + theme_ipsum_rc(grid = FALSE)
# p <- p + coord_cartesian(ylim=c(60.44,60.465), xlim=c(22.27,22.305)) #zoomaus
# p6 <- p + theme(axis.text = element_blank(),
#                 axis.title = element_blank())
# p6

# Näiden alueiden ulkopuolelle jää vain Skanssi, joka on toistaiseksi kilometrejä muista erillään.