# Jaetaan Turku muutamaan alueeseen, vielä kesken
# Skanssi on kaikista muista kilometrejä erillään, joten siellä vain yksi teline
# Popup-asemat voi siirtyä muutaman kuukauden välein, tarkista nämä toisinaan
library(tidyverse)
library(jsonlite)
library(plyr)

loop_data <- data_frame(nimi = c("Satama", "Portsa_Kakola", "Keskusta", 
                                 "Tuomiokirkko", "Yliopisto", "Kupittaa_kunnallissairaala", "Skanssi"
),
pisteet = c(list(c("Satama", "Linna ", "Forum Marinum")),
            list(c("Varvintori","Popup 3 ", "Föri ", "Port Arthur", "Puistokatu", "Vaakahuone", "Vesibussi", "Martti")),
            list(c("Päärautatieasema", "Humalistonkatu", "Ursininkatu", "Kaupunginteatteri", "Eerikinkatu",
                   "Kävelykatu", "Kristiinankatu", "Kaupungintalo")),
            list(c("Linja-autoasema", "Puutori", "Brahenkatu", "Vähätori", "Kirjastosilta", "Piispankatu", 
                   "Tuomiokirkko")),
            list(c("Ikituuri", "Assistentinpolku", "Popup 1", "T-sairaala ", "Hämeenkatu", "Kurjenkaivonkenttä",
                  "Kupittaan asema", "Datacity")),
            list(c("Kupittaankenttä", "Kaskenkatu ", "Kunnallissairaala")),
            list(c("Skanssi"))
            
            
),
# Nämä määrittelyt ovatkin varmaan turhia, koska ne automaattisesti lasketaan myöhemmin, mutta pidetään kuitenkin
latmax = c(60.443, 60.451, 60.4565, 60.458, 60.4617, 60.448, 60.435),
latmin = c(60.43, 60.437, 60.443, 60.445, 60.447, 60.435, 60.422),
lonmax = c(22.24, 22.256, 22.2685, 22.282, 22.301, 22.285, 22.32),
lonmin = c(22.215, 22.239, 22.253, 22.267, 22.284, 22.27, 22.305)
)



# 
# # Jos uusia alueita, luo niille uudet pohjakarttadatat!!
# 
uusi_alue = FALSE
if (uusi_alue){
  for (i in 1:nrow(loop_data)){

    # readRDS("./data/df16_digiroad_uusimaa2.RDS") %>%
    #   filter(KUNTAKOODI == "91",
    #          lat >= loop_data[i,]$latmin,
    #          lat <= loop_data[i,]$latmax,
    #          long >= loop_data[i,]$lonmin,
    #          long <= loop_data[i,]$lonmax) %>%
    #   saveRDS(., paste0("./data/tiet_",loop_data[i,]$nimi,".RDS"))

    readRDS("./data/turku_tiet2.RDS") %>%
      filter(lat >= loop_data[i,]$latmin,
             lat <= loop_data[i,]$latmax,
             long >= loop_data[i,]$lonmin,
             long <= loop_data[i,]$lonmax) %>%
      saveRDS(., paste0("./data/tiet_",loop_data[i,]$nimi,".RDS"))

  }
}